import {plugModule} from "./gva";
import * as express from "express";
import graphqlHTTP = require("express-graphql");
import {Server} from "duniter/server";
import * as http from "http";

export function gvaHttpListen(server: Server, port: number, host = 'localhost'): http.Server {
  const app = express()
  app.use('/graphql', graphqlHTTP({
    schema: plugModule(server),
    graphiql: true,
  }))
  return app.listen(port, host, () => console.log(`Now browse to ${host}:${port}/graphql`))
}
