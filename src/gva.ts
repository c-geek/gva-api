import * as path from "path";
import * as fs from "fs";
import {Server} from "duniter/server";
import {makeExecutableSchema} from "graphql-tools";
import {FullIindexEntry, FullMindexEntry} from "duniter/app/lib/indexer";
import {hashf} from "duniter/app/lib/common";

export function plugModule(server: Server) {
  const schemaFile = path.join(__dirname, 'schema.graphqls');
  const typeDefs = fs.readFileSync(schemaFile, 'utf8');
  return makeExecutableSchema({
    typeDefs,
    resolvers: {

      Query: {
        hello: () => 'Welcome to Duniter GVA API.',

        currency: () => server.conf.currency,

        block: async (_, { number }: { number?: number }) => {
          if (number !== undefined) {
            return server.dal.getBlock(number)
          }
          const b = await server.dal.getCurrentBlockOrNull()
          return b
        },

        member: async (_, { uid, pub }: { uid: string, pub: string }) => {
          if (uid) {
            return server.dal.iindexDAL.getFullFromUID(uid)
          }
          return server.dal.iindexDAL.getFromPubkey(pub)
        },

        pendingIdentities: async (_, { search }: { search: string }) => {
          if (!search) {
            return server.dal.idtyDAL.getPendingIdentities()
          }
          return server.dal.idtyDAL.searchThoseMatching(search)
        },

        pendingIdentityByHash: async (_, { hash }: { hash: string }) => {
          return server.dal.idtyDAL.getByHash(hash)
        },

        pendingTransactions: async () => {
          return server.dal.txsDAL.getAllPending(1)
        },

        transactionByHash: async (_, { hash }: { hash: string }) => {
          return server.dal.txsDAL.getTX(hash)
        },

        transactionsOfIssuer: async (_, { issuer }: { issuer: string }) => {
          return (await server.dal.txsDAL.getLinkedWithIssuer(issuer))
            .concat(await server.dal.txsDAL.getPendingWithIssuer(issuer))
        },

        transactionsOfReceiver: async (_, { receiver }: { receiver: string }) => {
          return (await server.dal.txsDAL.getLinkedWithRecipient(receiver))
            .concat(await server.dal.txsDAL.getPendingWithRecipient(receiver))
        },

        sourcesOfPubkey: async (_, { pub }: { pub: string }) => {
          const txSources = await server.dal.sindexDAL.getAvailableForPubkey(pub)
          const udSources = await server.dal.dividendDAL.getUDSources(pub)
          const sources: {
            type: string
            noffset: number
            identifier: string
            amount: number
            base: number
            conditions: string
            consumed: boolean
          }[] = []
          txSources.forEach(s => sources.push({
            type: 'T',
            identifier: s.identifier,
            noffset: s.pos,
            amount: s.amount,
            base: s.base,
            conditions: s.conditions,
            consumed: false
          }))
          udSources.forEach(s => sources.push({
            type: 'D',
            identifier: pub,
            noffset: s.pos,
            amount: s.amount,
            base: s.base,
            conditions: `SIG(${pub})`,
            consumed: false
          }))
          return sources
        },

      },

      Identity: {
        certsIssued: async (identity: FullIindexEntry) => {
          return server.dal.cindexDAL.getValidLinksFrom(identity.pub)
        },
        certsReceived: async (identity: FullIindexEntry) => {
          return server.dal.cindexDAL.getValidLinksTo(identity.pub)
        },
        pendingIssued: async (identity: FullIindexEntry) => {
          return server.dal.certDAL.getFromPubkeyCerts(identity.pub)
        },
        pendingReceived: async (identity: FullIindexEntry) => {
          return server.dal.certDAL.getNotLinkedToTarget(identity.hash)
        },
        membership: async (identity: { pub:string }) => {
          const ms = (await server.dal.mindexDAL.getReducedMS(identity.pub)) as FullMindexEntry
          return {
            revokes_on: ms.revokes_on,
            expires_on: ms.expires_on,
            chainable_on: ms.chainable_on,
          }
        },
      },

      PendingIdentity: {
        certs: async (identity: { hash:string }) => {
          return server.dal.certDAL.getNotLinkedToTarget(identity.hash)
        },
        memberships: async (identity: { hash:string }) => {
          return server.dal.msDAL.getPendingINOfTarget(identity.hash)
        },
      },

      Mutation: {
        submitIdentity(_, { raw }: { raw: string }) {
          return server.writeRawIdentity(raw)
        },
        async submitCertification(_, { raw }: { raw: string }) {
          const res = await server.writeRawCertification(raw)
          const targetHash = hashf(res.idty_uid + res.idty_buid + res.idty_issuer)
          return server.dal.idtyDAL.getByHash(targetHash)
        },
        async submitMembership(_, { raw }: { raw: string }) {
          const res = await server.writeRawMembership(raw)
          const targetHash = hashf(res.userid + res.blockstamp + res.pub)
          return server.dal.idtyDAL.getByHash(targetHash)
        },
        async submitTransaction(_, { raw }: { raw: string }) {
          return server.writeRawTransaction(raw)
        },
      }
    }
  })
}