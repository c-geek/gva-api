# Setup

## Prérequis

* NodeJS 9

## Installation

### Dépendances

    yarn
    
### Synchronisation Ğ1

    node index.js sync g1.duniter.org
    
## Usage

    node index.js gva
    
L'IHM est alors consultable à l'adresse http://localhost:15000/graphql

## Exemples de requêtes

### hello

```graphql
{
  hello
}
```

```json
{
  "data": {
    "hello": "Welcome to Duniter GVA API."
  }
}
```

### block(number)

```graphql
{
  block(number: 0) {
    number
    currency
    membersCount
  }
}
```

```json
{
  "data": {
    "block": {
      "number": 0,
      "currency": "g1",
      "membersCount": 59
    }
  }
}
```

## API

Dans GraphQLi, dans le menu en haut à droite "Docs > Query" se situe la liste des opérations possibles actuelles.

```
hello: String
currency: String!
block(number: Int): Block
member(uid: Stringpub: String): Identity
pendingIdentities(search: String): [PendingIdentity!]!
pendingIdentityByHash(hash: String!): PendingIdentity
pendingTransactions: [Transaction!]!
transactionByHash(hash: String!): Transaction
transactionsOfIssuer(issuer: String!): [Transaction!]!
transactionsOfReceiver(receiver: String!): [Transaction!]!
sourcesOfPubkey(pub: String!): [Source!]!
``` 
