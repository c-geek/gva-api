import * as requestPromise from "request-promise";
import {DBBlock} from "duniter/app/lib/db/DBBlock";
import {DBIdentity} from "duniter/app/lib/dal/sqliteDAL/IdentityDAL";

export class TestGvaClient {

  constructor(protected uri: string) {}

  graphQLPost(req: string) {
    return requestPromise(this.uri, {
      method: 'POST',
      json: true,
      body: {
        query: req
      }
    })
  }

  async currency(): Promise<string> {
    return (await this.graphQLPost(`{ currency }`) as any).data.currency
  }

  async current(): Promise<DBBlock|null> {
    const res = await (await this.graphQLPost(`{ block {
      version
      number
      currency
      hash
      inner_hash
      signature
      previousHash
      issuer
      previousIssuer
      time
      powMin
      unitbase
      membersCount
      issuersCount
      issuersFrame
      issuersFrameVar
      identities
      joiners
      actives
      leavers
      revoked
      excluded
      certifications
      transactions {
        version
        currency
        locktime
        hash
        blockstamp
        blockstampTime
        issuers
        inputs
        outputs
        unlocks
        signatures
        comment
      }
      medianTime
      nonce
      parameters
      monetaryMass
      dividend
      UDTime
      writtenOn
      written_on
    } }`) as any)
    return res.data.block
  }

  hello() {
    return this.graphQLPost('{ hello }')
  }

  submitIdentity(raw: string) {
    return this.graphQLPost(`mutation {
      submitIdentity(raw: "${raw.replace(/\n/g, '\\n')}") {
        revoked
        buid
        member
        kick
        leaving
        wasMember
        pubkey
        uid
        sig
        revocation_sig
        hash
        written
        revoked_on
        expires_on
      }
    }`)
  }

  submitCertification(raw: string) {
    return this.graphQLPost(`mutation {
      submitCertification(raw: "${raw.replace(/\n/g, '\\n')}") {
        revoked
        buid
        member
        kick
        leaving
        wasMember
        pubkey
        uid
        sig
        revocation_sig
        hash
        written
        revoked_on
        expires_on
        certs {
          linked
          written
          written_block
          written_hash
          sig
          block_number
          block_hash
          target
          to
          from
          block
          expired
          expires_on
        }
        memberships {
          membership
          issuer
          number
          blockNumber
          blockHash
          userid
          certts
          block
          fpr
          idtyHash
          written
          written_number
          expires_on
          signature
          expired
          block_number
        }
      }
    }`)
  }

  submitMembership(raw: string) {
    return this.graphQLPost(`mutation {
      submitMembership(raw: "${raw.replace(/\n/g, '\\n')}") {
        revoked
        buid
        member
        kick
        leaving
        wasMember
        pubkey
        uid
        sig
        revocation_sig
        hash
        written
        revoked_on
        expires_on
        certs {
          linked
          written
          written_block
          written_hash
          sig
          block_number
          block_hash
          target
          to
          from
          block
          expired
          expires_on
        }
        memberships {
          membership
          issuer
          number
          blockNumber
          blockHash
          userid
          certts
          block
          fpr
          idtyHash
          written
          written_number
          expires_on
          signature
          expired
          block_number
        }
      }
    }`)
  }

  async submitTransaction(raw: string) {
    return (await this.graphQLPost(`mutation {
      submitTransaction(raw: "${raw.replace(/\n/g, '\\n')}") {
        hash
        block_number
        locktime
        version
        currency
        comment
        blockstamp
        blockstampTime
        time
        inputs
        unlocks
        outputs
        issuers
        signatures
        written
        removed
        received
        output_base
        output_amount
        written_on
        writtenOn
      }
    }`))
  }

  async pendingIdentities(search = ''): Promise<DBIdentity[]> {
    return (await this.graphQLPost(`{
      pendingIdentities(search: "${search}") {
        revoked
        buid
        member
        kick
        leaving
        wasMember
        pubkey
        uid
        sig
        revocation_sig
        hash
        written
        revoked_on
        expires_on
        certs {
          linked
          written
          written_block
          written_hash
          sig
          block_number
          block_hash
          target
          to
          from
          block
          expired
          expires_on
        }
        memberships {
          membership
          issuer
          number
          blockNumber
          blockHash
          userid
          certts
          block
          fpr
          idtyHash
          written
          written_number
          expires_on
          signature
          expired
          block_number
        }
      }
    }`) as any).data.pendingIdentities
  }

  async sourcesOfPubkey(pub: string): Promise<{
    type: string
    noffset: number
    identifier: string
    amount: number
    base: number
    conditions: string
    consumed: boolean
  }[]> {
    return (await this.graphQLPost(`{
      sourcesOfPubkey(pub: "${pub}") {
        type
        noffset
        identifier
        amount
        base
        conditions
        consumed
      }
    }`) as any).data.sourcesOfPubkey
  }
}