import * as assert from "assert"
import {NewTestingServer, TestingServer} from "duniter/test/integration/tools/toolbox";
import {gvaHttpListen} from "../src/network";
import {Underscore} from "duniter/app/lib/common-libs/underscore";
import {TestUser} from "duniter/test/integration/tools/TestUser";
import {TestGvaClient} from "./test-gva-client";
import * as http from "http";
import {GvaTestUser} from "./test-user";
import {DBBlock} from "duniter/app/lib/db/DBBlock";

export const prepareDuniterServer = async (options:any) => {

  const catKeyring = { pub: 'HgTTJLAQ5sqfknMq7yLPZbehtuLSsKj9CxWN7k8QvYJd', sec: '51w4fEShBk1jCMauWu4mLpmDVfHksKmWcygpxriqCEZizbtERA6de4STKRkQBpxmMUwsKXRjSzuQ8ECwmqN1u2DP'};
  const tacKeyring = { pub: '2LvDg21dVXvetTD9GdkPLURavLYEqP3whauvPWX4c2qc', sec: '2HuRLWgKgED1bVio1tdpeXrf7zuUszv1yPHDsDj7kcMC4rVSN9RC58ogjtKNfTbH1eFz7rn38U1PywNs3m6Q7UxE'};

  const s1 = NewTestingServer(Underscore.extend({ pair: catKeyring }, options || {}));

  const cat = new TestUser('cat', catKeyring, { server: s1 });
  const tac = new TestUser('tac', tacKeyring, { server: s1 });

  await s1._server.initWithDAL()

  return { s1, cat, tac };
}

describe('GVA', () => {

  const port = 15000
  const now = 1480000000

  let s1: TestingServer
  let cat: GvaTestUser
  let tac: GvaTestUser
  let gva: http.Server
  let gvaClient: TestGvaClient

  before(async () => {
    const { s1: _s1, cat: _cat, tac: _tac } = await prepareDuniterServer({
      dt: 1000,
      ud0: 200,
      udTime0: now - 1, // So we have a UD right on block#1
      medianTimeBlocks: 1 // Easy: medianTime(b) = time(b-1)
    })
    s1 = _s1
    gva = gvaHttpListen(s1._server, port)
    gvaClient = new TestGvaClient(`http://localhost:${port}/graphql`)
    cat = new GvaTestUser(
      _cat.uid,
      _cat.pub,
      _cat.sec,
      gvaClient
    )
    tac = new GvaTestUser(
      _tac.uid,
      _tac.pub,
      _tac.sec,
      gvaClient
    )
  })

  it('GVA should answer', async () => {
    const res = await gvaClient.hello()
    assert.deepEqual(res.data, {
      "hello": "Welcome to Duniter GVA API."
    })
    assert.equal(null, await gvaClient.current())
  })

  it('cat & tac should be able to create its identity', async () => {
    await cat.createIdentityAndSubmit()
    assert.equal(1, (await gvaClient.pendingIdentities()).length)
    await tac.createIdentityAndSubmit()
    assert.equal(2, (await gvaClient.pendingIdentities()).length)
  })

  it('cat & tac should be able to certify each other', async () => {
    await cat.createCertAndSubmit(tac)
    assert.equal(1, (await gvaClient.pendingIdentities())[1].certs.length)
    assert.equal(0, (await gvaClient.pendingIdentities())[0].certs.length)
    await tac.createCertAndSubmit(cat)
    assert.equal(1, (await gvaClient.pendingIdentities())[1].certs.length)
    assert.equal(1, (await gvaClient.pendingIdentities())[0].certs.length)
  })

  it('cat & tac should be able to submit membership', async () => {
    await cat.createJoinAndSubmit()
    assert.equal(1, ((await gvaClient.pendingIdentities())[0] as any).memberships.length)
    assert.equal(0, ((await gvaClient.pendingIdentities())[1] as any).memberships.length)
    await tac.createJoinAndSubmit()
    assert.equal(1, ((await gvaClient.pendingIdentities())[0] as any).memberships.length)
    assert.equal(1, ((await gvaClient.pendingIdentities())[1] as any).memberships.length)
  })

  it('root block should be generated', async () => {
    await s1.commit({ time: now })
    const current = await gvaClient.current()
    assert.notEqual(null, current)
    assert.equal(0, (current as DBBlock).number)
    assert.equal(cat.pub, (current as DBBlock).issuer)
    assert.equal(0, (current as DBBlock).issuersCount)
  })

  it('UD should be available at block#1', async () => {
    await s1.commit({ time: now })
    const current = await gvaClient.current()
    assert.notEqual(null, current)
    assert.equal(1, (current as DBBlock).number)
    assert.equal(200, (current as DBBlock).dividend)
  })

  it('cat & tac should both have 200 units', async () => {
    assert.equal(200, (await cat.balance()))
    assert.equal(200, (await tac.balance()))
  })

  it('cat should be able to send money to tac', async () => {
    await cat.createTxAndSubmit(tac, 100)
    await s1.commit({ time: now })
    const current = await gvaClient.current()
    assert.notEqual(null, current)
    assert.equal(2, (current as DBBlock).number)
    assert.equal(null, (current as DBBlock).dividend)
    assert.equal(1, (current as DBBlock).transactions.length)
  })

  it('cat should have 100 units', async () => {
    assert.equal(100, (await cat.balance()))
  })

  it('tac should have 300 units', async () => {
    assert.equal(300, (await tac.balance()))
  })

  after(() => Promise.all([
    s1.closeCluster(),
    new Promise(res => gva.close(res)),
  ]))
})
